# Count the number of alphabets, numbers(digits) and spaces in a string. Example below.
"""
String = Pycharm 1997
alphabets = 7
digits = 4
spaces = 1
"""

def main():
    string = input("Please enter a valid string: ")
    alphabets = 0
    digits = 0
    spaces = 0
    for character in string:
        if (character>='A' and character<='Z') or (character>='a' and character<='z'):
            alphabets += 1
        elif character>='0' and character<='9':
            digits += 1
        elif character == ' ':
            spaces += 1
    print("Number of Spaces: {}".format(spaces))
    print("Number of digits: {}".format(digits))
    print("Number of alphabets: {}".format(alphabets))


if __name__ == '__main__':
    main()