# Find all the prime Numbers between 2 to N. Example Below
"""
N = 10
Answer = 2, 3, 5, 7 i.e All the prime numbers between 2 to 10
"""

def main():
    N = int(input("Please enter the upper boundary: "))
    for i in range(2, N+1):
        flag = True
        for j in range(2, i):
            if i % j == 0:
                flag = False
                break
        if flag:
            print(i, end=' ')

if __name__ == '__main__':
    main()