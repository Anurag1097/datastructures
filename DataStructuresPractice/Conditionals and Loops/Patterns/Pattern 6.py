# Print the following pattern
"""
1
11
121
1221
"""

def main():
    N = int(input("Please enter the number of rows: "))
    N -= 1
    print("1", end='\n')
    for i in range(1, N+1):
        print("1", end='')
        for j in range(0, i-1):
            print(2, end='')
        print("1", end='\n')

if __name__ == '__main__':
    main()