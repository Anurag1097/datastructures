# Print the following pattern
"""
A
BB
CCC
DDDD
"""

def main():
    N = int(input("Please enter the number of rows: "))
    character = 'A'
    for i in range(1, N+1):
        for j in range(0, i):
            print(character, end='')
        print('', end='\n')
        character = chr(ord(character) + 1)


if __name__ == '__main__':
    main()