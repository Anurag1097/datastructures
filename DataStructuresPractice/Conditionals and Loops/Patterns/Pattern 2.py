# Print the following pattern
"""
   1
  23
 345
4567
"""

def main():
    N = int(input("Please enter an integer for number of rows in pattern: "))
    for i in range(1, N+1):
        j = i
        for k in range(0, N-i):
            print(' ', end='')
        for k in range(0, i):
            if k == i-1:
                print(j, end='\n')
            else:
                print(j, end='')
                j += 1

if __name__ == '__main__':
    main()