# Print the following pattern
"""
   *
  ***
 *****
*******
"""

def main():
    N = int(input("Please enter the number of rows: "))
    for i in range(1, N+1):
        for k in range(0, N-i):
            print(' ', end='')
        for k in range(0, i):
            if i == 1:
                print('*', end='\n')
            else:
                print('*', end='')
        for k in range(0, i-1):
            if k == i-2:
                print('*', end='\n')
            else:
                print('*', end='')


if __name__ == '__main__':
    main()