# Print the following pattern
"""
1234
123
12
1
"""

def main():
    N = int(input("Please enter the number of rows: "))
    for i in range(N, 0, -1):
        for j in range(1, i+1):
            print(j, end='')
        print('', end='\n')


if __name__ == '__main__':
    main()