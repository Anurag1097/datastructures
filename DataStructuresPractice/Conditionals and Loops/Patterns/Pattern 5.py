# Print the following pattern
"""
1
11
202
3003
"""

def main():
    N = int(input("Please enter the number of rows: "))
    N -= 1
    print("1", end='\n')
    for i in range(1, N+1):
        print(i, end='')
        for j in range(0, i-1):
            print("0", end='')
        print(i, end='\n')


if __name__ == '__main__':
    main()