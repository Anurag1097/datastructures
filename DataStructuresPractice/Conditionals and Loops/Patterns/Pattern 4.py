# Print the below pattern
"""
1
11
111
1111
"""

def main():
    N = int(input("Please enter the number of rows: "))
    for i in range(1, N+1):
        for j in range(0, i):
            if j == i-1:
                print("1", end='\n')
            else:
                print("1", end='')

if __name__ == '__main__':
    main()