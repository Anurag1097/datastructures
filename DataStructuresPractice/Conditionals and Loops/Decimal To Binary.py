# Convert the decimal number to Binary. Example Below.
"""
Decimal = 5
Binary = 101
Logic => 5%2 = 1, 2%2 = 0 ... => 101
"""

def reverseNumber(number):
    reverse = 0
    while number > 0:
        digit = number % 10
        number = int(number/10)
        reverse = reverse*10 + digit
    return reverse

def main():
    decimal = int(input("Please enter the decimal number: "))
    binary = 0
    while decimal > 0:
        remainder = decimal % 2
        decimal = int(decimal/2)
        binary = binary*10 + remainder
    print("Binary Value is {}".format(reverseNumber(binary)))

if __name__ == '__main__':
    main()