# Calculate the sum of even and odd digits in a integer. Below is an example
"""
Num = 123456
SumEven => 2+4+6 = 12
SumOdd => 1+3+5 = 9
"""

def main():
    number = int(input("Please enter a number: "))
    sumEven = 0
    sumOdd = 0
    while number > 0:
        digit = number % 10
        number = int(number/10)
        if digit % 2 == 0:
            sumEven += digit
        else:
            sumOdd += digit
    print("Even Sum: {}".format(sumEven))
    print("Odd Sum: {}".format(sumOdd))


if __name__ == '__main__':
    main()