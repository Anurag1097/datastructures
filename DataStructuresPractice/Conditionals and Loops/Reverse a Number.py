# Reverse a given Number. Example below.
"""
number = 123456
reversedNumber = 654321
"""

def main():
    number = int(input("Please enter the number to be reversed: "))
    reversed_number = 0
    while number > 0:
        digit = number % 10
        number = int(number/10)
        reversed_number = reversed_number*10 + digit
    print(reversed_number)


if __name__ == '__main__':
    main()