# Calculate the total salary as per below. Also round off to nearest integer
"""
Total Salary = basic + hra + da + allowance -pf
hra = 0.2 * basic
da = 0.5 * basic
pf = 0.11 * basic
allowance = 1700 if group=A
allowance = 1500 if group=B
allowance = 1300 if group other than A or B
"""

def main():
    basic = int(input("Please enter the basic salary: "))
    group = input("Please enter the group: ")
    hra = 0.2 * basic
    da = 0.5 * basic
    pf = 0.11 * basic
    if group=='A':
        allowance = 1700
    elif group=='B':
        allowance = 1500
    else:
        allowance = 1300
    totalSalary = basic + hra + da + allowance - pf
    if totalSalary+0.5 >= int(totalSalary+1):
        print ("Total Salary: {}".format(int(totalSalary+1)))
    else:
        print ("Total Salary: {}".format(int(totalSalary)))

if __name__ == '__main__':
    main()