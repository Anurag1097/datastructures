# Find nth power of a number. Example below
"""
2^3 = 8 i.e. 2 raised to the power 3 is 8
"""

def main():
    number = int(input("Please enter a number: "))
    power = int(input("Please enter the power: "))
    value = 1
    for i in range(power, 0, -1):
        value *= number
    print(value)


if __name__ == '__main__':
    main()