# Convert a binary number to its decimal value. Example below.
"""
Binary Number = 101
Decimal value = 5
Logic => (1 * 2^0) + (0 * 2^1) + (1 * 2^2) = 1 + 0 + 4
"""

def main():
    binary = int(input("Please enter the binary number: "))
    decimal = 0
    power = 0
    while binary > 0:
        digit = binary % 10
        binary = int(binary/10)
        decimal += digit * pow(2, power)
        power += 1
    print("Decimal value is {}".format(decimal))

if __name__ == '__main__':
    main()