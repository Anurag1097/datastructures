# This python script will convert Fahrenheit temperature to Celcius within a range with specified increase.


def main():
    start = int(input("Please enter the start temperature: "))
    end = int(input("Please enter the end temperature: "))
    jump = int(input("Please enter the jump per single increase: "))
    for i in range(start,end+1,jump):
        celcius = (i-32)*5/9
        print("Fahrenheit:{fh}, Celcius:{ch}".format(fh=i,ch=celcius))


if __name__=='__main__':
    main()