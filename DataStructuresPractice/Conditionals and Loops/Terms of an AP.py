# Print the first N terms of an AP which are divisible by 4. Example Below.
"""
N = 4
nth term = 3n+2
So the first four terms are as follows:
n=1 => 5
n=2 => 8
n=3 => 11
n=4 => 14
n=5 => 17
n=6 => 20
......
n=10 => 32
n=14 => 44
Therefore the answer is 8, 20, 32, 44
"""

def main():
    N = int(input("Please enter the number of terms: "))
    count = 0
    i = 1
    while count < N:
        term = 3*i + 2
        if term % 4 == 0:
            count += 1
            print(term, end=' ')
        i += 1


if __name__ == '__main__':
    main()