# Write a script to find Nth Fibonacci Number. Example below.
"""
Fibonacci Series = 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, ... i.e. every element is the sum of previous two numbers
4th Fibonacci Number = 2
"""

def main():
    N = int(input("Please enter the place of fibonacci number to be found: "))
    prev2 = 0
    prev1 = 1
    for i in range(3, N+1):
        nth = prev1 + prev2
        prev2 = prev1
        prev1 = nth
    print(nth)
if __name__ == '__main__':
    main()