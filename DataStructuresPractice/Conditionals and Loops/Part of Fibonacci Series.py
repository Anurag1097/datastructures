# Check whether the given number is a part of Fibonacci Serires or not. Example below.
"""
Logic => For a given number n, if any of the two integers, (5*n*n)+4  or (5*n*n)-4, is a perfect square, then the number is a part of fibonacci series.
N = 5
(5*N*N) + 4 = 129
(5*N*N) - 4 = 121
Now out of 129 and 121, 121 is a perfect square, then 5 is a part of Fibonacci Series
"""

def isPerfectSquare(number):
    interim = 0
    while(interim*interim <= number):
        interim += 1
    if (interim-1)*(interim-1) == number:
        return True
    else:
        return False

def main():
    N = int(input("Please enter the integer to be checked: "))
    num1 = 5*pow(N, 2) + 4
    num2 = 5*pow(N, 2) - 4
    flag1 = isPerfectSquare(num1)
    flag2 = isPerfectSquare(num2)
    if flag1 or flag2:
        print("{} is a part of Fibonacci Series".format(N))
    else:
        print("{} is not a part of Fibonacci Series".format(N))

if __name__ == '__main__':
    main()