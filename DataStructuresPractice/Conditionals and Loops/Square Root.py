# Find square root of a number. Example below.
"""
number = 4
answer = 2
Logic => 2*2 = 4
"""

def main():
    number = int(input("Please enter the number whose square root is required: "))
    answer = 0
    while(answer*answer <= number):
        answer += 1
    print("Square Root is: {}".format(answer-1))

if __name__ == '__main__':
    main()