# Find the sum or product of digits of a number based on user choice. Example below
"""
Number = 1234
if choice = sum, then answer = 1+2+3+4 = 10
if choice = product, then answer = 1*2*3*4 = 24
"""

def main():
    choice = input("Please enter your choice, valid options are sum/product: ")
    number = int(input("Please enter a number: "))
    if choice == 'sum':
        sum = 0
        while number > 0:
            digit = number % 10
            number = int(number/10)
            sum += digit
        print("Sum of the digits: {}".format(sum))
    elif choice == 'product':
        product = 1
        while number > 0:
            digit = number % 10
            number = int(number/10)
            product *= digit
        print("Product of digits: {}".format(product))
    else:
        print("Not a valid option")

if __name__ == '__main__':
    main()