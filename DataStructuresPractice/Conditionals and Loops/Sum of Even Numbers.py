# This script will calculate the sum of all even numbers till an integer N given as user input

def main():
    N = int(input("Please enter an integer: "))
    sum = 0
    for i in range(1,N+1):
        if i%2==0:
            sum+=i
    print(sum)

if __name__=='__main__':
    main()