# check whether the entered character is uppercase or lowercase

def main():
    character = input("Please enter a character: ")
    if character>='A' and character<='Z':
        print("{} is an uppercase character.".format(character))
    elif character>='a' and character<='z':
        print("{} is a lowercase character.".format(character))
    else:
        print("Invalid entry. This {} is not a character.".format(character))

if __name__== '__main__':
    main()