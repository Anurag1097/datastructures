# Find unique element in the array. Example below.
"""
array = [1,2,1,3,3,4,6,4,4]
answer = 2 i.e. it is the only element occurring once on the array
"""


def main():
    array = (input("Please enter the space separated elements of the array: ")).split(' ')
    array = [int(x) for x in array]
    result = {}
    for digit in array:
        if digit in result:
            result[digit] += 1
        else:
            result[digit] = 1
    for key in result:
        if result[key] == 1:
            print("Unique Element is: {}".format(key))
            break


if __name__ == '__main__':
    main()