# Find the duplicate element in an array
"""
array = [1,2,3,4,5,6,1,7,8]
answer = 1 i.e. The only element occurring twice is 1.
"""


def main():
    array = (input("Please enter space separated of an array: ")).split(' ')
    array = [int(x) for x in array]
    seen = []
    for digit in array:
        if digit in seen:
            print("Duplicate digit is: {}".format(digit))
            break
        else:
            seen.append(digit)



if __name__ == '__main__':
    main()