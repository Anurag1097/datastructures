# Find duplicate element in the array. Example below.
"""
array = [1,2,3,4,5,6,1,7]
answer = 1
"""

def main():
    array = (input("Please enter space separated elements of the array: ")).split(' ')
    array = [int(x) for x in array]
    seen = []
    for digit in array:
        if digit in seen:
            break
        else:
            seen.append(digit)
    print("Repeating digit is {}".format(digit))


if __name__ == '__main__':
    main()