# Find the intersection of two arrays i.e. the common elements in both the arrays. Example below.
"""
array1 = [1,2,3,4,5,6,7,7]
array2 = [5,6,7,7,8]
intersection = [5,6,7,7]
"""


def main():
    array1 = (input("Please enter elements of array 1: ")).split(' ')
    array2 = (input("Please enter elements of array 2: ")).split(' ')
    array1 = [int(x) for x in array1]
    array2 = [int(x) for x in array2]
    count_dict = {}
    intersection = []
    for digit in array1:
        if digit in count_dict:
            count_dict[digit] += 1
        else:
            count_dict[digit] = 1
    for digit in array2:
        if digit in count_dict:
            intersection.append(digit)
            count_dict[digit] -= 1
    print("Intersection: {}".format(intersection))


if __name__ == '__main__':
    main()