# Check whether the number sequence is first strictly decreasing and then increasing. Example below.
"""
Number Sequence = [5,4,3,2,8,9,16], answer = True
Logic => This part of the sequence [5,4,3,2] is strictly decreasing and the other part [8, 9, 10] is strictly increasing
"""

def main():
    sequenceString = input("Please enter a space separated list of integers: ")
    sequence = sequenceString.split(' ')
    sequence = [int(x) for x in sequence]
    flag = 0
    for i in range(1, len(sequence)):
        if flag == 0:
            if sequence[i] >= sequence[i-1]:
                flag += 1
        elif flag == 1:
            if sequence[i] <= sequence[i-1]:
                flag += 1
        else:
            break
    if (flag == 0) or (flag == 1):
        print("True")
    else:
        print("False")

if __name__ == '__main__':
    main()