#  Arrange the numbers in an array such that all the odd numbers to the left and even numbers to the right. Example below.
"""
array = [1,2,3,4,5,6]
revised array = [1,3,5,4,6,4,2]
Logic => if num%2 == 0, then push it ot the end else push it to the left
"""

def main():
    array = (input("Please enter the space separated digits of an array: ")).split(' ')
    array = [int(x) for x in array]
    start = 0
    end = len(array) - 1
    revised_array = [0]*len(array)
    for digit in array:
        if digit%2 == 0:
            revised_array[end] = digit
            end -= 1
        else:
            revised_array[start] = digit
            start += 1
    print(revised_array)


if __name__ == '__main__':
    main()