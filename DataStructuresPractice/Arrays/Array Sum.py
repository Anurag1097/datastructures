# Find the sum of all the elements in an 1-D array. Example Below.
"""
array = [1, 2, 3, 4, 5, 6, 7, 8, 9]
sum = 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 = 45
"""

def main():
    sequence = (input("Please enter the space separated array elements: ")).split(' ')
    sequence = [int(item) for item in sequence]
    sum = 0
    for item in sequence:
        sum += item
    print("Sum of all elements: {}".format(sum))

if __name__ == '__main__':
    main()