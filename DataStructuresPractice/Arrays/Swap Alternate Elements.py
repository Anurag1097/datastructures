# Swap alternate elements of an array. Example below.
"""
array = [1,2,3,4,5,6]
revised_array = [2,1,4,3,6,5]
"""

def main():
    array = (input("Please enter space separated elements of an array: ")).split(' ')
    array = [int(x) for x in array]
    for i in range(0, len(array)-1, 2):
        temp = array[i]
        array[i] = array[i+1]
        array[i+1] = temp
    print("Revised array is: {}".format(array))


if __name__ == '__main__':
    main()