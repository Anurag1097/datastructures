# Linear search for an element in an array and return the index of that element. Example Below.
"""
array = [1,2,3,6,7,16,12,5,4,9]
x = 5
index = 7
"""

def main():
    array = (input("Please enter the space separated array of integers: ")).split(' ')
    array = [int(x) for x in array]
    print(array)
    key = int(input("Please enter the element to be searched: "))
    for i in range(0, len(array)):
        if array[i] == key:
            break
    if i == len(array) - 1 and array[i] != key :
        print("Element not found: {}".format(key))
    else:
        print("Required index is: {}".format(i))



if __name__ == '__main__':
    main()